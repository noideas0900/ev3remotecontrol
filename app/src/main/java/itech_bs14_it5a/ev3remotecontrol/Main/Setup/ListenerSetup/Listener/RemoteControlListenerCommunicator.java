package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener;

import java.util.ArrayList;
import java.util.List;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

public class RemoteControlListenerCommunicator {

    private List<IListenerObserver> _observers = new ArrayList<>();

    public void registerNewObserver(IListenerObserver observer) {
        if(!_observers.contains(observer))
            _observers.add(observer);
    }

    public void notifyObservers(int pressedButtonId, Object pushedData) {
        for (IListenerObserver observer:_observers) {
            observer.processIncomingRemoteControlCommand(pressedButtonId, pushedData);
        }
    }
}
