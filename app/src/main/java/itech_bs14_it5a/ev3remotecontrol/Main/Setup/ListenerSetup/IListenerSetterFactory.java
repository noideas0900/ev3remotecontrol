package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import java.util.ArrayList;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter.IListenerSetter;

interface IListenerSetterFactory {
    IListenerSetter makeListenerSetterFactory(String factoryType);
    ArrayList<String> getFactoryTypes();
}
