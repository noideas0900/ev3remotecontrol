package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter;

import android.view.View;
import android.widget.SeekBar;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;

public class SeekbarListenerSetter implements IListenerSetter {


    @Override
    public void setListener(RemoteControlListener listener, View view) {
        if(IsViewCompatibleWithListenerSetter(view)) {
            SeekBar seekBar = (SeekBar) view;
            SetMatchingListener(seekBar, listener);
        }
    }

    @Override
    public boolean IsViewCompatibleWithListenerSetter(View view) {
        return view instanceof SeekBar;
    }


    private void SetMatchingListener(SeekBar seekBar, RemoteControlListener listener) {
        SeekBar.OnSeekBarChangeListener currentListener =
                (SeekBar.OnSeekBarChangeListener) listener;
        seekBar.setOnSeekBarChangeListener(currentListener);
    }
}



