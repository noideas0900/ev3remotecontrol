package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnValueChanged.IpChange;

import android.view.View;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

/**
 * Created by Blubb on 09.10.2017.
 */

public class IpListener extends RemoteControlListener implements View.OnClickListener{


    public IpListener(IListenerObserver remoteControl, int pressedButtonId) {
        super(remoteControl, pressedButtonId);

    }

    @Override
    public void onClick(View v) {
        notifyObservers(_pressedButtonId, IpAddressListener.Ip);
    }
}
