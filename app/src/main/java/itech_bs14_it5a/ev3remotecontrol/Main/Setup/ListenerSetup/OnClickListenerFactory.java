package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import android.view.View;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnClick.RemoteControlOnClickListener;
import itech_bs14_it5a.ev3remotecontrol.R;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

class OnClickListenerFactory implements IListenerTypeFactory {


    private IListenerObserver _remoteControl;

    public OnClickListenerFactory(IListenerObserver remoteControl) {
        _remoteControl = remoteControl;
    }


    public RemoteControlListener makeListener(View view) {
        int pressedButtonId = view.getId();
        switch (pressedButtonId) {
            case R.id.Stop:
                return new RemoteControlOnClickListener(_remoteControl, pressedButtonId);
            case R.id.Up:
                return new RemoteControlOnClickListener(_remoteControl, pressedButtonId);
            case R.id.Right:
                return new RemoteControlOnClickListener(_remoteControl, pressedButtonId);
            case R.id.Down:
                return new RemoteControlOnClickListener(_remoteControl, pressedButtonId);
            case R.id.Left:
                return new RemoteControlOnClickListener(_remoteControl, pressedButtonId);
            case R.id.RotateLeft:
                return new RemoteControlOnClickListener(_remoteControl, pressedButtonId);
            case R.id.RotateRight:
                return new RemoteControlOnClickListener(_remoteControl, pressedButtonId);
            default:
                return null;
        }
    }
}
