package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter;

import android.view.View;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;

/**
 * Created by Bernd_nmhfyo5 on 05.10.2017.
 */

public interface IListenerSetter {
    void setListener(RemoteControlListener listener, View view);
    boolean IsViewCompatibleWithListenerSetter(View view);
}
