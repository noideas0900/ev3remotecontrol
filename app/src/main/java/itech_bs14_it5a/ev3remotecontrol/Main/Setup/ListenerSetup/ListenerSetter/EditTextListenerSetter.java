package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter;

import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;

public class EditTextListenerSetter implements IListenerSetter {

    @Override
    public void setListener(RemoteControlListener listener, View view) {
        if (IsViewCompatibleWithListenerSetter(view)) {
            EditText editText = (EditText) view;
            SetMatchingListener(editText, listener);
        }
    }

    @Override
    public boolean IsViewCompatibleWithListenerSetter(View view) {
        return view instanceof EditText;
    }

    private void SetMatchingListener(EditText editText, RemoteControlListener listener) {
        TextWatcher currentListener = (TextWatcher) listener;
        editText.addTextChangedListener(currentListener);
    }


}





