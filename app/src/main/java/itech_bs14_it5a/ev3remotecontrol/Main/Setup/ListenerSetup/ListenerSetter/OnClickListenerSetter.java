package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter;

import android.view.View;
import android.widget.ImageButton;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;

public class OnClickListenerSetter implements IListenerSetter {

    @Override
    public void setListener(RemoteControlListener listener, View view) {
        if(IsViewCompatibleWithListenerSetter(view)) {
            ImageButton imageButton = (ImageButton) view;
            SetMatchingListener(imageButton, listener);
        }
    }

    @Override
    public boolean IsViewCompatibleWithListenerSetter(View view) {
        return view instanceof ImageButton;
    }

    private void SetMatchingListener(ImageButton imageButton, RemoteControlListener listener) {
        View.OnClickListener currentListener = (View.OnClickListener) listener;
        imageButton.setOnClickListener(currentListener);
    }
}
