package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.Main.Factory.Factory;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

class ListenerTypeFactoryMaker extends Factory implements IListenerTypeFactoryMaker {

    private final String _onClickListenerFactory = "onClick";
    private final String _onValueChangedListenerFactory = "onValueChanged";
    private IListenerObserver _remoteControl;

    public ListenerTypeFactoryMaker(IListenerObserver remoteControl) {
        super();
        _remoteControl = remoteControl;
    }

    @Override
    public ArrayList<String> getFactoryTypes() {
        return _allFactoryTypes;
    }

    @Override
    public IListenerTypeFactory makeListenerTypeFactory(String factoryType) {
        switch (factoryType) {
            case _onClickListenerFactory:
                return new OnClickListenerFactory(_remoteControl);
            case _onValueChangedListenerFactory:
                return new OnValueChangedListenerFactory(_remoteControl);
            default:
                return null;
        }
    }

    @Override
    public void setupFactoryTypeList() {
        _allFactoryTypes.add(_onClickListenerFactory);
        _allFactoryTypes.add(_onValueChangedListenerFactory);
    }
}
