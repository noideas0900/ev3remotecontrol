package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnValueChanged.SpeedChange;

import android.widget.SeekBar;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

public class SpeedListener extends RemoteControlListener implements SeekBar.OnSeekBarChangeListener {

    public SpeedListener(IListenerObserver remoteControl, int pressedButtonId) {
        super(remoteControl, pressedButtonId);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        notifyObservers(_pressedButtonId, progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {


    }
}
