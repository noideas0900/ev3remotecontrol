package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter.EditTextListenerSetter;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter.IListenerSetter;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter.NullListenerSetter;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter.OnClickListenerSetter;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter.SeekbarListenerSetter;
import itech_bs14_it5a.ev3remotecontrol.Main.Factory.Factory;

class ListenerSetterFactory extends Factory implements IListenerSetterFactory {

    private final String _onClickListenerSetter = "onClick";
    private final String _seekbarListenerSetter = "seekbar";
    private final String _ipAddressListenerSetter = "ipa";

    public ListenerSetterFactory() {
        super();
    }

        public IListenerSetter makeListenerSetterFactory(String factoryType){
            switch(factoryType){
                case _onClickListenerSetter:
                    return new OnClickListenerSetter();
                case _seekbarListenerSetter:
                    return new SeekbarListenerSetter();
                case _ipAddressListenerSetter:
                    return new EditTextListenerSetter();
                default:
                    return new NullListenerSetter();
            }
        }

    @Override
    public ArrayList<String> getFactoryTypes() {
        return _allFactoryTypes;
    }

    @Override
    public void setupFactoryTypeList() {
        _allFactoryTypes.add(_onClickListenerSetter);
        _allFactoryTypes.add(_seekbarListenerSetter);
        _allFactoryTypes.add(_ipAddressListenerSetter);
    }
}
