package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;


import android.view.View;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnValueChanged.IpChange.IpAddressListener;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnValueChanged.IpChange.IpListener;
import itech_bs14_it5a.ev3remotecontrol.R;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnValueChanged.SpeedChange.SpeedListener;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

class OnValueChangedListenerFactory implements IListenerTypeFactory {

    private IListenerObserver _remoteControl;

    public OnValueChangedListenerFactory(IListenerObserver remoteControl) {
        _remoteControl = remoteControl;
    }

    public RemoteControlListener makeListener(View view) {
        int pressedButtonId = view.getId();
        switch (pressedButtonId) {
            case R.id.Speed:
                return new SpeedListener(_remoteControl, pressedButtonId);
            case R.id.IpTxt:
                return new IpAddressListener(_remoteControl, pressedButtonId);
            case R.id.IpBtn:
                return new IpListener(_remoteControl, pressedButtonId);
            default:
                return null;
        }

    }
}
