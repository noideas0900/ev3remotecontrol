package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

public abstract class RemoteControlListener extends RemoteControlListenerCommunicator {

    protected int _pressedButtonId;

    public RemoteControlListener(IListenerObserver remoteControl, int pressedButtonId) {
        registerNewObserver(remoteControl);
        _pressedButtonId = pressedButtonId;
    }
}
