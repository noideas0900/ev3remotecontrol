package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnValueChanged.IpChange;

import android.text.Editable;
import android.text.TextWatcher;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

/**
 * Created by Blubb on 09.10.2017.
 */

public class IpAddressListener extends RemoteControlListener implements TextWatcher {

    public static String Ip = "";

    public IpAddressListener(IListenerObserver remoteControl, int pressedButtonId) {
        super(remoteControl, pressedButtonId);

    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        Ip = s.toString();
    }
}
