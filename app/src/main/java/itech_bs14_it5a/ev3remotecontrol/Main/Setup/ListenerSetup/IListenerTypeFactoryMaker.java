package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import java.util.ArrayList;

interface IListenerTypeFactoryMaker {

    ArrayList<String> getFactoryTypes();
    IListenerTypeFactory makeListenerTypeFactory(String factoryType);
}
