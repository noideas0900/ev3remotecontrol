package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import android.view.View;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;

interface IListenerTypeFactory {

    RemoteControlListener makeListener(View view);
}