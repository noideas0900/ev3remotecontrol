package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

 class ViewFinder{


    private ArrayList<View> _viewChecked;
    private ArrayList<View> _viewToBeChecked;
     private View _masterView;

    ViewFinder() {
        _viewChecked = new ArrayList<>();
        _viewToBeChecked = new ArrayList<>();
    }

     public ViewFinder(View view) {
         _masterView = view;
         _viewChecked = new ArrayList<>();
         _viewToBeChecked = new ArrayList<>();
     }

     ArrayList<View> findAllViews() {
        _viewToBeChecked.add(_masterView);
        FindAllViewsInMasterView();
        return _viewChecked;
    }

    private void FindAllViewsInMasterView() {
        while (!isMasterViewCheckedEntirely()) {
            ToggleCurrentViewChecked();
            if (!CanCurrentViewContainChildren()) continue;
            CheckCurrentViewForChildren();
        }
    }

    private boolean isMasterViewCheckedEntirely() {
        return _viewToBeChecked.isEmpty();
    }

    private void ToggleCurrentViewChecked() {
        _viewChecked.add(_viewToBeChecked.remove(0));
    }

    private boolean CanCurrentViewContainChildren() {
        return getCurrentView() instanceof ViewGroup;
    }

    private View getCurrentView() {
        return _viewChecked.get(_viewChecked.size() - 1);
    }

    private void CheckCurrentViewForChildren() {
        ViewGroup currentView = (ViewGroup) getCurrentView();
        final int childCount = currentView.getChildCount();
        AddPossibleChildrenToBeCheckedNext(currentView, childCount);
    }

    private void AddPossibleChildrenToBeCheckedNext(ViewGroup currentView, int childCount) {
        for (int i=0; i<childCount; i++)
            _viewToBeChecked.add(currentView.getChildAt(i));
    }


}




