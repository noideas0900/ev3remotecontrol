package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup;

import android.view.View;
import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.IApplicationSetup;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetter.IListenerSetter;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;


public class ListenerSetup implements IApplicationSetup {

    private ViewFinder _viewFinder;
    private IListenerTypeFactoryMaker _listenerTypeFactoryMaker;

    public ListenerSetup(View view, IListenerObserver remoteControl) {
        _viewFinder = new ViewFinder(view);
        _listenerTypeFactoryMaker = new ListenerTypeFactoryMaker(remoteControl);
    }

    @Override
    public void execute() {
        ArrayList<String> factoryTypes = _listenerTypeFactoryMaker.getFactoryTypes();
        BuildListenerTypeFactories(factoryTypes);
    }
    


    private void BuildListenerTypeFactories(ArrayList<String> factoryTypes){
        for (String factoryType:factoryTypes){
            IListenerTypeFactory listenerFactory = _listenerTypeFactoryMaker.makeListenerTypeFactory(factoryType);
            SetListenerForViews(listenerFactory);
        }
    }

    private void SetListenerForViews(IListenerTypeFactory listenerFactory) {
        ArrayList<View> allViews = _viewFinder.findAllViews();
        for (View view:allViews){
            CreateListener(view, listenerFactory);
        }
    }

    private void CreateListener(View view, IListenerTypeFactory listenerFactory) {
        RemoteControlListener listener = listenerFactory.makeListener(view);
        SetExistingListenerForView(listener, view);
    }

    private void SetExistingListenerForView(RemoteControlListener listener, View view) {
        if(listener != null){
            SetListenerForView(listener, view);
        }
    }


    private void SetListenerForView(RemoteControlListener listener, View view){
        IListenerSetterFactory listenerSetterFactory = new ListenerSetterFactory();
        ArrayList<String> factoryTypes = listenerSetterFactory.getFactoryTypes();
        BuildListenerSetter(listenerSetterFactory, factoryTypes,listener, view);
    }

    private void BuildListenerSetter(IListenerSetterFactory listenerSetterFactory,
                                     ArrayList<String> factoryTypes, RemoteControlListener listener, View view) {
        for (String factoryType:factoryTypes){
            IListenerSetter listenerSetter = listenerSetterFactory.makeListenerSetterFactory(factoryType);
            SetListener(listenerSetter, listener, view);
        }
    }

    private void SetListener(IListenerSetter listenerSetter, RemoteControlListener listener, View view) {
        listenerSetter.setListener(listener, view);
    }

}
