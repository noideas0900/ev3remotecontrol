package itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.OnClick;

import android.view.View;


import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListener;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.Listener.RemoteControlListenerCommunicator;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;
;

public class RemoteControlOnClickListener extends RemoteControlListener implements View.OnClickListener{

    public RemoteControlOnClickListener(IListenerObserver remoteControl, int pressedButtonId) {
        super(remoteControl, pressedButtonId);
    }

    @Override
    public void onClick(View v) {
        notifyObservers(_pressedButtonId, null);
    }
}
