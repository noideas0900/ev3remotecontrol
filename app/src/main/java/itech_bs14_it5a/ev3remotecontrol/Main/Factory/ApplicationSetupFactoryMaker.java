package itech_bs14_it5a.ev3remotecontrol.Main.Factory;

import android.view.View;
import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RemoteControl;


/**
 * Created by Bernd_nmhfyo5 on 05.10.2017.
 */

public class ApplicationSetupFactoryMaker extends Factory {

    private final String _viewFactory = "view";
    private final View _allViews;
    private IListenerObserver _remoteControl;

    public ApplicationSetupFactoryMaker(View allViews){
        super();
        _allViews = allViews;
        _remoteControl = new RemoteControl();
    }

    @Override
    public IApplicationSetupFactory makeFactory(String factoryType) {
        switch(factoryType){
            case _viewFactory:
                return new ViewSetupFactory(_allViews, _remoteControl);
            default:
                return new NullSetupFactory();
        }
    }

    public ArrayList<String> getFactoryTypes() {
        return _allFactoryTypes;
    }

    @Override
    public void setupFactoryTypeList() {
        _allFactoryTypes.add(_viewFactory);
    }
}
