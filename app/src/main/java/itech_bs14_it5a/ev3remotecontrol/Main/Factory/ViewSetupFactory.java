package itech_bs14_it5a.ev3remotecontrol.Main.Factory;

import android.view.View;

import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.IApplicationSetup;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.ListenerSetup.ListenerSetup;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.NullSetup;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IListenerObserver;

class ViewSetupFactory extends Factory implements IApplicationSetupFactory {

    private View _view;
    private final String _listenerSetup = "listenerSetup";
    private IListenerObserver _remoteControl;

    public ViewSetupFactory(View view, IListenerObserver remoteControl){
        super();
        _view = view;
        _remoteControl = remoteControl;
    }

    @Override
    public IApplicationSetup makeSetupFactory(String applicationSetupFactoryType) {
        switch(applicationSetupFactoryType){
            case _listenerSetup:
                return new ListenerSetup(_view, _remoteControl);
            default:
                return new NullSetup();
        }
    }

    @Override
    public ArrayList<String> getFactoryTypes() {
        return _allFactoryTypes;
    }

    @Override
    public void setupFactoryTypeList() {
        _allFactoryTypes.add(_listenerSetup);
    }
}
