package itech_bs14_it5a.ev3remotecontrol.Main.Factory;

import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.IApplicationSetup;
import itech_bs14_it5a.ev3remotecontrol.Main.Setup.NullSetup;


/**
 * Created by Bernd_nmhfyo5 on 05.10.2017.
 */

class NullSetupFactory implements IApplicationSetupFactory {
    @Override
    public IApplicationSetup makeSetupFactory(String applicationSetupFactoryType) {
        return new NullSetup();
    }

    @Override
    public ArrayList<String> getFactoryTypes() {
        return new ArrayList<>();
    }
}
