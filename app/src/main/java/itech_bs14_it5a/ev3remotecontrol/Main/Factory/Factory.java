package itech_bs14_it5a.ev3remotecontrol.Main.Factory;

import java.util.ArrayList;

public abstract class Factory {

    public ArrayList<String> _allFactoryTypes;

    public Factory() {
        _allFactoryTypes = new ArrayList<>();
        setupFactoryTypeList();
    }

    public void setupFactoryTypeList() {}

    public IApplicationSetupFactory makeFactory(String factoryType){
        return null;}

    public ArrayList<String> getFactoryTypes(){
        return _allFactoryTypes;
    }
}

