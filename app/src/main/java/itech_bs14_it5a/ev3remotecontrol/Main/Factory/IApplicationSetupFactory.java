package itech_bs14_it5a.ev3remotecontrol.Main.Factory;

import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.Main.Setup.IApplicationSetup;

public interface IApplicationSetupFactory {

    IApplicationSetup makeSetupFactory(String applicationSetupFactoryType);
    ArrayList<String> getFactoryTypes();
}
