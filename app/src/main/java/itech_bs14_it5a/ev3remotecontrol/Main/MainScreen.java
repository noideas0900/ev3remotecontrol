package itech_bs14_it5a.ev3remotecontrol.Main;

        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.EditText;

        import java.util.ArrayList;
        import itech_bs14_it5a.ev3remotecontrol.Main.Factory.ApplicationSetupFactoryMaker;
        import itech_bs14_it5a.ev3remotecontrol.Main.Factory.Factory;
        import itech_bs14_it5a.ev3remotecontrol.Main.Factory.IApplicationSetupFactory;
        import itech_bs14_it5a.ev3remotecontrol.R;
        import itech_bs14_it5a.ev3remotecontrol.Main.Setup.IApplicationSetup;


public class MainScreen extends AppCompatActivity {

    private Factory _mainFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        Setup();
        EditText text = (EditText) findViewById(R.id.IpTxt);
    }

    private void Setup() {
        SetupMainFactory();
        CreateApplicationSetupFactories();
    }

    private void SetupMainFactory() {
        View mainScreen = findViewById(android.R.id.content);
        _mainFactory = new ApplicationSetupFactoryMaker(mainScreen);
    }

    private void CreateApplicationSetupFactories() {
        ArrayList<String> factoryTypes = _mainFactory.getFactoryTypes();
        CreateApplicationSetupFactory(factoryTypes);
    }

    private void CreateApplicationSetupFactory(ArrayList<String> factoryTypes) {
        for (String factoryType:factoryTypes) {
            IApplicationSetupFactory applicationSetupFactory = _mainFactory.makeFactory(factoryType);
            CreateApplicationSetups(applicationSetupFactory);
        }
    }

    private void CreateApplicationSetups(IApplicationSetupFactory applicationSetupFactory) {
        ArrayList<String> factoryTypes = applicationSetupFactory.getFactoryTypes();
        CreateApplicationSetup(applicationSetupFactory, factoryTypes);
    }

    private void CreateApplicationSetup(IApplicationSetupFactory applicationSetupFactory, ArrayList<String> factoryTypes) {
        for (String factoryType:factoryTypes) {
            IApplicationSetup applicationSetup = applicationSetupFactory.makeSetupFactory(factoryType);
            performSetup(applicationSetup);
        }
    }

    private void performSetup(IApplicationSetup applicationSetup) {
        applicationSetup.execute();
    }

}
