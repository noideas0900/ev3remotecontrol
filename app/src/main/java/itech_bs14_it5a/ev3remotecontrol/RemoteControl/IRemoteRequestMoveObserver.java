package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;

public interface IRemoteRequestMoveObserver {
    void updateRemoteControlAfterMoveRequest(int pressedButtonId, Ev3 currentRobotState);
}
