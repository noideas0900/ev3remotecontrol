package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;

public interface IRemoteRequestObserver {
    void updateRemoteControlStatusAfterRequest(int currentlyPressedButtonId, Ev3 currentRobotState);
}
