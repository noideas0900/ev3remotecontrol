package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.IRequestType;

interface IRequestTypeFactory {

    IRequestType getRequestType(int ButtonId);
    IRequestType returnSpeedRequest();
}
