package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.IRequestType;


public class RemoteControl implements IRemoteRequestConnectObserver,
        IRemoteRequestMoveObserver, IRemoteRequestSpeedObserver, IListenerObserver {

    private Ev3 _robot;
    private IRequestTypeFactory _requestFactory;

    public RemoteControl() {
        _requestFactory = new RequestTypeFactory(this, this, this);
    }

    @Override
    public void processIncomingRemoteControlCommand(int buttonId, Object pushedData) {
        IRequestType requestType = _requestFactory.getRequestType(buttonId);
        requestType.makeRequest(buttonId, pushedData, _robot);
    }

    @Override
    public void updateRemoteControlSpeedAfterRequest(Ev3 currentRobotState) {
        _robot = currentRobotState;
    }

    @Override
    public void updateRemoteControlAfterConnect(Ev3 currentRobotState) {
        _robot = currentRobotState;
    }

    @Override
    public void updateRemoteControlAfterMoveRequest(int pressedButtonId, Ev3 currentRobotState) {
        _robot = currentRobotState;
        UpdateSpeedForMoveRequest(pressedButtonId);

    }

    private void UpdateSpeedForMoveRequest(int pressedButtonId) {
        IRequestType speedRequest = _requestFactory.returnSpeedRequest();
        speedRequest.makeRequest(pressedButtonId, null, _robot);
    }
}

