package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;


public interface IEv3RequestFactory {
    Ev3Request makeRequest(IRequestRobotObserver robotObserver,
                           int requestedActionButtonId);
}
