package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

public interface IListenerObserver {
    void processIncomingRemoteControlCommand(int buttonId, Object pushedData);
}
