package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

import java.util.ArrayList;

import itech_bs14_it5a.ev3remotecontrol.R;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect.ConnectRequestType;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.IRequestType;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.MovementRequestType;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.NullRequestType;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed.SpeedRequestType;

class RequestTypeFactory implements IRequestTypeFactory {

    private IRequestType _movementRequest;
    private IRequestType _connectRequest;
    private IRequestType _speedRequest;
    private IRequestType _nullRequest;

    public RequestTypeFactory(IRemoteRequestMoveObserver moveObserver,
                              IRemoteRequestSpeedObserver speedObserver,
                              IRemoteRequestConnectObserver connectObserver){
        setupRequestTypes(moveObserver, speedObserver, connectObserver);
    }

    @Override
    public IRequestType getRequestType(int buttonId) {
        switch(buttonId){
            case R.id.Up:
                return _movementRequest;
            case R.id.Down:
                return _movementRequest;
            case R.id.Left:
                return _movementRequest;
            case R.id.Right:
                return _movementRequest;
            case R.id.RotateLeft:
                return _movementRequest;
            case R.id.RotateRight:
                return _movementRequest;
            case R.id.IpBtn:
                return _connectRequest;
            case R.id.Speed:
                return _speedRequest;
            default:
                return _nullRequest;

        }
    }

    private void setupRequestTypes(IRemoteRequestMoveObserver moveObserver,
                                   IRemoteRequestSpeedObserver speedObserver,
                                   IRemoteRequestConnectObserver connectObserver) {
        _movementRequest = new MovementRequestType(moveObserver);
        _connectRequest = new ConnectRequestType(connectObserver);
        _speedRequest = new SpeedRequestType(speedObserver);
        _nullRequest = new NullRequestType();
    }

    @Override
    public IRequestType returnSpeedRequest() {
        return _movementRequest;
    }
}
