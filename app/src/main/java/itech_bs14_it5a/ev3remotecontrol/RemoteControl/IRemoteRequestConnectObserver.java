package itech_bs14_it5a.ev3remotecontrol.RemoteControl;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

public interface IRemoteRequestConnectObserver {
    void updateRemoteControlAfterConnect(Ev3 currentRobotState);
}
