package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IRemoteRequestSpeedObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.RobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.IRequestType;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

public class SpeedRequestType extends RemoteControlSpeedRequestCommunicator implements IRequestType {

    private ISpeedRequestFactory _speedFactory;
    private IRequestRobotObserver _requestObserver;

    public SpeedRequestType(IRemoteRequestSpeedObserver requestObserver) {
        registerNewObserver(requestObserver);
        _speedFactory = new SpeedFactory();
        _requestObserver = new RobotObserver();
    }

    @Override
    public void makeRequest(int pressedButtonId, Object value, Ev3 robot) {
        Ev3Request request = _speedFactory.makeRequest(pressedButtonId, value, _requestObserver);
        request.execute(robot);
        notifyObservers(_requestObserver.getCurrentRobotState());

    }
}
