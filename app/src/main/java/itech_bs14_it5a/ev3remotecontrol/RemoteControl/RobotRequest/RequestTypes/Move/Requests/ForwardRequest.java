package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;


public class ForwardRequest extends Ev3Request {

    public ForwardRequest(IRequestRobotObserver robotRequestObserver) {
        super(robotRequestObserver);
    }

    @Override
    protected Ev3 doInBackground(Ev3... robot) {
        do{
            currentEv3(robot).driveForward();
        } while (!isCancelled());
        return currentEv3(robot);
    }
}
