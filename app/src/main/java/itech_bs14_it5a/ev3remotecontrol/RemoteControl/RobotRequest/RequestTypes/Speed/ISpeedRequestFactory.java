package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

interface ISpeedRequestFactory {


    Ev3Request makeRequest(int pressedButtonId, Object value, IRequestRobotObserver requestObserver);
}
