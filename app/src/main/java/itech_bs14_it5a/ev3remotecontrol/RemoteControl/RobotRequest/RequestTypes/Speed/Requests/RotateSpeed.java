package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed.Requests;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;

public class RotateSpeed extends Ev3SpeedRequest {
    public RotateSpeed(IRequestRobotObserver requestObserver, Object newSpeed) {
        super(requestObserver, newSpeed);
    }

    @Override
    protected Ev3 doInBackground(Ev3... robot) {
        Ev3 ev3 = currentEv3(robot);
        int seekBarSpeed = DetermineCurrentSeekBarSpeed(ev3);
        UpdateRobotSpeed(ev3, seekBarSpeed);
        return ev3;
    }

    private int DetermineCurrentSeekBarSpeed(Ev3 robot) {
        if(isCurrentSpeedRequestNotMovementUpdate())
            robot.setSeekBarSpeed((Integer) _speedValue);
        return robot.getSeekBarSpeed();
    }

    private boolean isCurrentSpeedRequestNotMovementUpdate() {
        return _speedValue != null;
    }

    private void UpdateRobotSpeed(Ev3 ev3, int seekBarSpeed) {
        boolean isZero = IsExistingZeroSpeedSet(ev3, seekBarSpeed);
        if(!isZero)
            ConvertSpeed(ev3, seekBarSpeed);
    }

    private void ConvertSpeed(Ev3 ev3, int currentSeekBarSpeed) {
        double robotMaxSpeed = ev3.getRotateMaxSpeed();
        double result =
                (robotMaxSpeed / SeekBarMaxSpeedValue) * currentSeekBarSpeed;
        ev3.setSpeed(result);
    }

    private boolean IsExistingZeroSpeedSet(Ev3 ev3, int seekBarSpeed) {
        if(IsSpeedZero(seekBarSpeed)){
            ev3.setSpeed(0);
            return true;
        } return false;
    }

    private boolean IsSpeedZero(int updatedSpeedValue) {
        return updatedSpeedValue == 0;
    }

}
