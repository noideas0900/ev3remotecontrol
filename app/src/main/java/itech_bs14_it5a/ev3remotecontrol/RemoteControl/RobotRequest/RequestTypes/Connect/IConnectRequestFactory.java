package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect.Requests.ConnectRequest;

interface IConnectRequestFactory {
    ConnectRequest makeRequest(int pressedButtonId, Object value, IRequestRobotObserver requestObserver);
}
