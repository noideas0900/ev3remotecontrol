package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect.Requests;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import lejos.remote.ev3.RemoteRequestEV3;

public class ConnectRequest extends Ev3ConnectRequest {
    public ConnectRequest(IRequestRobotObserver requestObserver, Object value) {
        super(requestObserver, value);
    }

    @Override
    protected Ev3 doInBackground(Ev3... robot) {
        RemoteRequestEV3 connectedRobot;
        try {
            connectedRobot = new RemoteRequestEV3(_ipAddress);
        } catch (Exception ex) {
            return null;
        }
        return new Ev3(connectedRobot);
    }
}
