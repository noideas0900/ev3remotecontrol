package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed.Requests;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;

class Ev3SpeedRequest extends Ev3Request {

    protected int SeekBarMaxSpeedValue = 5;
    Object _speedValue;

    Ev3SpeedRequest(IRequestRobotObserver robotRequestObserver, Object speedValue) {
        super(robotRequestObserver);
        _speedValue = speedValue;
    }
}
