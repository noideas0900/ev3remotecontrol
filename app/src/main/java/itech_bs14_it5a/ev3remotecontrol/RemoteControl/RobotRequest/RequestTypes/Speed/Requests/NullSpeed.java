package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed.Requests;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;


public class NullSpeed extends Ev3Request {

    public NullSpeed(IRequestRobotObserver robotRequestObserver) {
        super(robotRequestObserver);
    }

    @Override
    protected Ev3 doInBackground(Ev3... robot) {
        return currentEv3(robot);
    }
}
