package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed;

import java.util.ArrayList;
import java.util.List;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IRemoteRequestSpeedObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;

class RemoteControlSpeedRequestCommunicator {


    private List<IRemoteRequestSpeedObserver> _observers = new ArrayList<>();

    public void registerNewObserver(Object observer) {
        if(!_observers.contains(observer))
            _observers.add((IRemoteRequestSpeedObserver) observer);
    }

    public void notifyObservers(Ev3 currentRobotState) {
        for (IRemoteRequestSpeedObserver observer:_observers) {
            observer.updateRemoteControlSpeedAfterRequest(currentRobotState);
        }
    }
}
