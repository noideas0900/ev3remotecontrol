package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move;

import itech_bs14_it5a.ev3remotecontrol.R;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IEv3RequestFactory;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.BackwardRequest;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.ForwardRequest;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.RotateLeftRequest;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.RotateRightRequest;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.StopRequest;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.TurnLeftRequest;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.TurnRightRequest;


public class MovementFactory implements IEv3RequestFactory {

    @Override
    public Ev3Request makeRequest(IRequestRobotObserver robotObserver,
                                  int requestedActionButtonId) {

        switch (requestedActionButtonId) {
            case R.id.Down:
                return new BackwardRequest(robotObserver);
            case R.id.Up:
                return new ForwardRequest(robotObserver);
            case R.id.RotateLeft:
                return new RotateLeftRequest(robotObserver);
            case R.id.RotateRight:
                return new RotateRightRequest(robotObserver);
            case R.id.Stop:
                return new StopRequest(robotObserver);
            case R.id.Left:
                return new TurnLeftRequest(robotObserver);
            case R.id.Right:
                return new TurnRightRequest(robotObserver);
            default:
                return null;
        }
    }
}
