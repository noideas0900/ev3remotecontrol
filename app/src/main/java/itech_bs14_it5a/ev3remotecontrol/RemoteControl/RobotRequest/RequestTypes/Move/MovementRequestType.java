package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IEv3RequestFactory;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IRemoteRequestMoveObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.RobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.IRequestType;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests.StopRequest;

public class MovementRequestType extends RemoteControlMoveRequestCommunicator implements IRequestType {

    private IRequestRobotObserver _robotObserver;
    private IEv3RequestFactory _requestFactory;
    private final int NoRequestCurrentlyPerformed = 0;
    private int _buttonIdOfCurrentlyPerformedRequest = NoRequestCurrentlyPerformed;
    private Ev3Request _currentAction;

    public MovementRequestType(IRemoteRequestMoveObserver requestObserver) {
        _requestFactory = new MovementFactory();
        _robotObserver = new RobotObserver();
        registerNewObserver(requestObserver);
    }

    @Override
    public void makeRequest(int pressedButtonId, Object value, Ev3 robot) {
        if(robot == null){
            return;
        };
        StopRequestOnLostConnection(robot);
        _robotObserver.setCurrentRobotState(robot);
        DetermineRequest(pressedButtonId);
    }//testcomment

    private void StopRequestOnLostConnection(Ev3 robot) {
        if(!robot.isConnected()){
            ResetRemoteControl();
        }
    }

    private void DetermineRequest(int pressedButtonId) {
        if(_robotObserver.getCurrentRobotState().isConnected()){
            PerformRequest(pressedButtonId);
        }
    }

    private void PerformRequest(int pressedButtonId) {
        Ev3Request request = _requestFactory.makeRequest(_robotObserver, pressedButtonId);
        ExecuteRequestIfNecessary(request, pressedButtonId);
    }

    private void ExecuteRequestIfNecessary(Ev3Request request, int pressedButtonId) {
        if(isCurrentRequestCancelRequest(request)){
            ResetRemoteControl();
            notifyObservers(_robotObserver.getCurrentRobotState(), pressedButtonId);
        } else ExecuteRequest(request, pressedButtonId);
    }

    private boolean isCurrentRequestCancelRequest(Ev3Request request) {
        return request instanceof StopRequest;
    }

    private void ResetRemoteControl() {
        CancelExistingFormerRequest();
        _buttonIdOfCurrentlyPerformedRequest = NoRequestCurrentlyPerformed;
        _currentAction = null;
    }

    private void CancelExistingFormerRequest() {
        if(_currentAction != null)
            _currentAction.cancel(true);
    }

    private void ExecuteRequest(Ev3Request request, int pressedButtonId) {
        if(!IsRequestAlreadyBeingPerformed(pressedButtonId))
            UpdateRemoteControl(request, pressedButtonId);
            DoRequest();
    }

    private boolean IsRequestAlreadyBeingPerformed(int pressedButtonId) {
        return pressedButtonId == _buttonIdOfCurrentlyPerformedRequest;
    }

    private void DoRequest() {
        CancelExistingFormerRequest();
        _currentAction.execute(_robotObserver.getCurrentRobotState());
    }



    private void UpdateRemoteControl(Ev3Request request, int pressedButtonId) {
        _currentAction = request;
        _buttonIdOfCurrentlyPerformedRequest = pressedButtonId;
        notifyObservers(_robotObserver.getCurrentRobotState(), pressedButtonId);
    }
}
