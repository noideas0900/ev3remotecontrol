package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed;

import itech_bs14_it5a.ev3remotecontrol.R;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed.Requests.NullSpeed;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed.Requests.RotateSpeed;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Speed.Requests.StraightSpeed;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

class SpeedFactory implements ISpeedRequestFactory {


    @Override
    public Ev3Request makeRequest(int pressedButtonId, Object value, IRequestRobotObserver requestObserver) {
        switch(pressedButtonId){
            case R.id.Up:
                return new StraightSpeed(requestObserver, value);
            case R.id.Down:
                return new StraightSpeed(requestObserver, value);
            case R.id.Left:
                return new StraightSpeed(requestObserver, value);
            case R.id.Right:
                return new StraightSpeed(requestObserver, value);
            case R.id.RotateLeft:
                return new RotateSpeed(requestObserver, value);
            case R.id.RotateRight:
                return new RotateSpeed(requestObserver, value);
            default:
                return new NullSpeed(requestObserver);
        }
    }
}
