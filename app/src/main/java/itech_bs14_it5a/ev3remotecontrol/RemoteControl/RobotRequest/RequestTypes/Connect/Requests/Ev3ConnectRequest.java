package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect.Requests;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

class Ev3ConnectRequest extends Ev3Request {

    String _ipAddress;

    Ev3ConnectRequest(IRequestRobotObserver robotRequestObserver, Object value) {
        super(robotRequestObserver);
        SetIp(value);
    }

    private void SetIp(Object value) {
        _ipAddress = (String) value;
    }
}
