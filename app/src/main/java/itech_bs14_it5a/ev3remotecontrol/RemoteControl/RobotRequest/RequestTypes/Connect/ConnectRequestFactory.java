package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect;

import itech_bs14_it5a.ev3remotecontrol.R;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect.Requests.ConnectRequest;


class ConnectRequestFactory implements IConnectRequestFactory {
    @Override
    public ConnectRequest makeRequest(int pressedButtonId, Object value, IRequestRobotObserver requestObserver) {
        switch(pressedButtonId){
            case R.id.IpBtn:
                return new ConnectRequest(requestObserver, value);
            default:
                return null;
        }
    }
}
