package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move.Requests;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

class Ev3MoveRequest extends Ev3Request {
    protected Ev3MoveRequest(IRequestRobotObserver robotRequestObserver) {
        super(robotRequestObserver);
    }

    @Override
    protected void onCancelled(Ev3 result) {
        super.onCancelled(result);
        result.stopCurrentAction();
        notifyObservers(result);
    }
}
