package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Move;

import java.util.ArrayList;
import java.util.List;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IRemoteRequestMoveObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;

class RemoteControlMoveRequestCommunicator {

    private List<IRemoteRequestMoveObserver> _observers = new ArrayList<>();

    public void registerNewObserver(Object observer) {
        if(!_observers.contains(observer))
            _observers.add((IRemoteRequestMoveObserver) observer);
    }

    public void notifyObservers(Ev3 currentRobotState, int pressedButtonId) {
        for (IRemoteRequestMoveObserver observer:_observers) {
            observer.updateRemoteControlAfterMoveRequest(pressedButtonId, currentRobotState);
        }
    }
}
