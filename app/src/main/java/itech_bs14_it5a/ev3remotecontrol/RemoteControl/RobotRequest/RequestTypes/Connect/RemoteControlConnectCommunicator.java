package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect;

import java.util.ArrayList;
import java.util.List;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IRemoteRequestConnectObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;

/**
 * Created by Bernd_nmhfyo5 on 08.10.2017.
 */

class RemoteControlConnectCommunicator {


    private List<IRemoteRequestConnectObserver> _observers = new ArrayList<>();

    public void registerNewObserver(Object observer) {
        if(!_observers.contains(observer))
            _observers.add((IRemoteRequestConnectObserver) observer);
    }

    public void notifyObservers(Ev3 currentRobotState) {
        for (IRemoteRequestConnectObserver observer:_observers) {
            observer.updateRemoteControlAfterConnect(currentRobotState);
        }
    }
}
