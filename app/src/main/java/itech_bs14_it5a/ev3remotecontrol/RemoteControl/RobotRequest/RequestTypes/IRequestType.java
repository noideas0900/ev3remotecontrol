package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;

public interface IRequestType {

    void makeRequest(int pressedButtonId, Object value, Ev3 robot);
}
