package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Connect;


import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.Ev3Request;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.IRemoteRequestConnectObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.RobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes.IRequestType;

public class ConnectRequestType extends RemoteControlConnectCommunicator implements IRequestType {

    private IConnectRequestFactory _connectFactory;
    private IRequestRobotObserver _requestObserver;


    public ConnectRequestType(IRemoteRequestConnectObserver requestObserver) {
        registerNewObserver(requestObserver);
        _connectFactory = new ConnectRequestFactory();
        _requestObserver = new RobotObserver();
    }

    @Override
    public void makeRequest(int pressedButtonId, Object value, Ev3 robot) {
        _requestObserver.setCurrentRobotState(robot);
        Ev3Request request = _connectFactory.makeRequest(pressedButtonId, value, _requestObserver);
        request.execute();
        notifyObservers(_requestObserver.getCurrentRobotState());

    }
}
