package itech_bs14_it5a.ev3remotecontrol.RemoteControl.RobotRequest.RequestTypes;

import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.Ev3;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.IRequestRobotObserver;
import itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot.RobotRequestCommunicator;

public abstract class Ev3Request extends RobotRequestCommunicator{

    protected Ev3Request(IRequestRobotObserver robotRequestObserver){
        registerNewObserver(robotRequestObserver);
    }

    protected void onPostExecute(Ev3 result) {
        notifyObservers(result);
    }

    protected Ev3 currentEv3(Ev3... robot) {
        return robot[0];
    }
}
