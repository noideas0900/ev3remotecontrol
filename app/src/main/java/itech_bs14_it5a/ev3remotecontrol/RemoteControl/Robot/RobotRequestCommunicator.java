package itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot;


import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;

public class RobotRequestCommunicator extends AsyncTask<Ev3, Integer, Ev3> {
    private List<IRequestRobotObserver> _observers = new ArrayList<>();

    public void registerNewObserver(IRequestRobotObserver observer) {
        if(!_observers.contains(observer))
            _observers.add(observer);
    }

    public void notifyObservers(Ev3 result) {
        for (IRequestRobotObserver observer:_observers) {
            observer.setCurrentRobotState(result);
        }
    }

    @Override
    protected Ev3 doInBackground(Ev3... robot) {
        return null;
    }

}
