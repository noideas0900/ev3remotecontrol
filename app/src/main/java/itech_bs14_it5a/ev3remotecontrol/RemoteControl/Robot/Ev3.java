package itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot;

import lejos.remote.ev3.RemoteRequestEV3;
import lejos.robotics.navigation.ArcRotateMoveController;

public class Ev3 {

    private ArcRotateMoveController _navigator;
    private RemoteRequestEV3 _brick;
  //  private RegulatedMotor _leftMotor;
  //  private RegulatedMotor _rightMotor;
    private double  _speed;
    private int _seekBarSpeed;

    private final String LeftMotorPort = "A";
    private final String RightMotorPort = "B";

    public Ev3(RemoteRequestEV3 connectedRobot){
        _brick = connectedRobot;
        setupNavigation();
        _speed = _navigator.getMaxTravelSpeed();
    }

    private void setupNavigation() {
      //  _leftMotor  = _brick.createRegulatedMotor(LeftMotorPort, 'L');
      //  _rightMotor = _brick.createRegulatedMotor(RightMotorPort, 'L');
        _navigator = _brick.createPilot(5.5, 5.5, LeftMotorPort, RightMotorPort);
    }
    public boolean isConnected() {
        return _brick != null;
    }

    public void stopCurrentAction() {
            _navigator.stop();
    }

    public void driveBackward() {
            _navigator.setTravelSpeed(_speed);
            _navigator.backward();
    }

    public void driveForward() {
            _navigator.setTravelSpeed(_speed);
            _navigator.forward();

    }

    public void rotateLeft() {
            _navigator.setRotateSpeed(_speed);
            _navigator.rotate(1);
    }

    public void rotateRight() {
            _navigator.setRotateSpeed(_speed);
            _navigator.rotate(-1);
    }


    public void turnLeft() {
            _navigator.setTravelSpeed(_speed);
            _navigator.arcForward(10);
    }

    public void turnRight() {
            _navigator.setTravelSpeed(_speed);
            _navigator.arcForward(-10);
    }

    public double getRotateMaxSpeed(){
        return _navigator.getRotateMaxSpeed();
    }

    public double getMaxTravelSpeed(){
        return _navigator.getMaxTravelSpeed();
    }

    public void setSpeed(double speed){
        _speed = speed;
    }

    public int getSeekBarSpeed() {return _seekBarSpeed;}

    public void setSeekBarSpeed(int seekBarSpeed) {
        _seekBarSpeed = seekBarSpeed;
    }
}
