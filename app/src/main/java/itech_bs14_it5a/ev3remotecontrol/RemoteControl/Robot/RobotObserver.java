package itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot;

public class RobotObserver implements IRequestRobotObserver {

    private Ev3 _currentRobotState;

    @Override
    public void setCurrentRobotState(Ev3 currentRobotState) {
        _currentRobotState = currentRobotState;
    }

    @Override
    public Ev3 getCurrentRobotState() {
        return _currentRobotState;
    }
}
