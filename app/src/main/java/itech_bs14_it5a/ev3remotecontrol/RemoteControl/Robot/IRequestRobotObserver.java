package itech_bs14_it5a.ev3remotecontrol.RemoteControl.Robot;

public interface IRequestRobotObserver {
    void setCurrentRobotState(Ev3 currentRobotState);
    Ev3 getCurrentRobotState();
}
